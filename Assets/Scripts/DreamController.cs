﻿using System.Collections;
using Me.StefS.UnityUtility;
using Me.StefS.UnityUtility.MVC;
using UnityEngine;

public class DreamController : Controller
{
    PlayerModel playerModel;
    protected override Model DefaultObjectWhenNoModelIsPresent()
    {
        return PlayerModel.ZERO_LEVEL;
    }

    protected override SceneType GeteSceneForWhichThisControllerIs()
    {
        return SceneType.DreamScene;
    }

    protected override void InitializeModel(Model model)
    {
        this.playerModel = (PlayerModel)model;
    }

    protected override IEnumerator OnStart()
    {
        return Corutines.WaitForSeconds(7f).Then(Corutines.Execute(() => Navigator.GoToScene(SceneType.TheAwakeningScene, this.playerModel)));
    }

    public void Update()
    {
        if (Input.anyKey)
            Navigator.GoToScene(SceneType.TheAwakeningScene, this.playerModel);
    }
}