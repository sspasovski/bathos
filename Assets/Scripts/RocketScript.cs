﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketScript : MonoBehaviour
{
    private ShipTiltController controller;
    void Awake()
    {
        controller = FindObjectOfType<ShipTiltController>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        controller.Dead();
        
        // play explosion animation
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
}
