﻿using UnityEngine;
using Me.StefS.UnityUtility.MVC;
using System.Collections;
using Me.StefS.UnityUtility;
using UnityEngine.UI;

public class TheAwakeningController : Controller
{
    PlayerModel playerModel;
    public Sprite[] sprites;
    private int nextSpriteIndex=0;
    public Image UIimage;
    protected override Model DefaultObjectWhenNoModelIsPresent()
    {
        return PlayerModel.ZERO_LEVEL;
    }

    protected override SceneType GeteSceneForWhichThisControllerIs()
    {
        return SceneType.TheAwakeningScene;
    }

    protected override void InitializeModel(Model model)
    {
        this.playerModel = (PlayerModel)model;
    }

    protected override IEnumerator OnStart()
    {
        NextSprite();
        return null;
        //return Corutines.WaitForSeconds(3f).Then(Corutines.Execute(() => Navigator.GoToScene(SceneType.GravityPullScene, this.playerModel)));
    }

    public void Update()
    {
        if (Input.anyKeyDown)
        {
            NextSprite();
        }
    }

    public void NextSprite()
    {
        if (nextSpriteIndex < sprites.Length)
        {
            UIimage.sprite = sprites[nextSpriteIndex];
            nextSpriteIndex++;
        }
        else
        {
            Navigator.GoToScene(SceneType.GravityPullScene, this.playerModel);
        }
    }
}
