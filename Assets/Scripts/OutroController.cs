﻿using System.Collections;
using System.Collections.Generic;
using Me.StefS.UnityUtility;
using Me.StefS.UnityUtility.MVC;
using UnityEngine;
using UnityEngine.UI;

public class OutroController : Controller
{
    PlayerModel playerModel;

    public Image image;
    public Sprite sprite;

    private Sprite oldSprite;
    private bool firstTimeChanged = false;

    protected override Model DefaultObjectWhenNoModelIsPresent()
    {
        return PlayerModel.ZERO_LEVEL;
    }

    protected override SceneType GeteSceneForWhichThisControllerIs()
    {
        return SceneType.AtmosphereEntranceScene;
    }

    protected override void InitializeModel(Model model)
    {
        this.playerModel = (PlayerModel)model;
    }

    protected override IEnumerator OnStart()
    {
        //Navigator.GoToScene(SceneType.ShipTiltScene, this.playerModel);
        yield return null;
    }

    private void Awake()
    {
        oldSprite = image.sprite;
    }

    private void Start()
    {
        image.sprite = oldSprite;
    }

    private void Update()
    {
        if (Input.anyKeyDown)
        {
            if (firstTimeChanged)
            {
                
                image.sprite = sprite;
            }
            else firstTimeChanged = true;

        }
    }
}
