﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public static readonly float SHIP_OFFSET = 0.0f;// 0.6f;
    public static float FLING_SCALE = 140f / 3f;
    public static float TRAIL_SCALE = 1f;
    public static float DISTANCE_SCALE = 1f / 10f;
    public static float MAX_GRAVITY_DECLINE = 60f;
    public static float MAX_FORCE = 2f;
    public static float TAKE_OFF_DICLINE_IN_SECONDS = 1f;
    public static float SHIP_MASS = 10f; //for inertia
    public static float LINE_WIDTH = 0.3f;
}
