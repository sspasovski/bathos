﻿using System.Collections;
using System.Collections.Generic;
using Me.StefS.Utility;
using UnityEngine;

public class    LineMovement : MonoBehaviour
{
    protected const float MaxDistance = 6;

    public LineRenderer Line;
    public Transform Rocket;

    public float Tolerance = 0;

    private Touch[] _touches;
    private float _leftPositionTouch;
    private float _rightPositionTouch;

    private Vector3 _movePosition;
    private float _lastPositionY;
    private float _speedOnLastUpdate;

    private Vector3[] _linePositions;


    void Start()
    {
        _linePositions = new[] { Vector3.zero, Vector3.zero };
        Line.GetPositions(_linePositions);
        _movePosition = new Vector2(Rocket.position.x, (_linePositions[0].y - (_linePositions[0].y - _linePositions[1].y)) - Rocket.position.x);
        _lastPositionY = Rocket.position.y;
    }

    void Update()
    {
        if (Rocket == null) return;

        TouchInput();
        RocketMovement();
    }

    void LateUpdate()
    {
        if (Rocket == null) return;

        _speedOnLastUpdate = (Vector3.Distance(_linePositions[0], _linePositions[1]) * Time.deltaTime) / (_lastPositionY < Rocket.position.y ? 4.56f : 2.43f);
        _lastPositionY = Rocket.position.y;
    }

    private void TouchInput()
    {
        _touches = Input.touches;

        for (var i = 0; i < Input.touchCount; i++)
        {
            Vector2 touchPos = getWorldPoint(_touches[i].position);

            Debug.Log(touchPos);

            switch (_touches[i].phase)
            {
                case TouchPhase.Began:

                    if (touchPos.x > 0) _rightPositionTouch = touchPos.y;
                    else _leftPositionTouch = touchPos.y;
                    break;

                case TouchPhase.Moved:
                    float distance;
                    if (touchPos.x > 0)
                    {
                        distance = _rightPositionTouch - touchPos.y;
                        if (distance < 1)
                        {
                            _rightPositionTouch = touchPos.y;
                            _linePositions[0] = new Vector3(_linePositions[0].x, Mathf.Clamp(_linePositions[0].y - distance,-4.5f,6.7f));
                            Line.SetPositions(_linePositions);
                        }
                    }
                    else
                    {
                        distance = _leftPositionTouch - touchPos.y;
                        if (distance < 1)
                        {
                            _leftPositionTouch = touchPos.y;
                            _linePositions[1] = new Vector3(_linePositions[1].x, Mathf.Clamp(_linePositions[1].y - distance, -4.5f, 6.7f));
                            Line.SetPositions(_linePositions);
                        }
                    }
                    break;
            }
        }
    }

    private void RocketMovement()
    {
        SetRocketPosition();
        SetRocketRotation();
    }

    private void SetRocketPosition()
    {
        if (_linePositions[0].y > _linePositions[1].y + Tolerance)
        {
            _movePosition = _linePositions[1];
        }
        else if (_linePositions[0].y + Tolerance < _linePositions[1].y)
        {
            _movePosition = _linePositions[0];
        }
        else
        {
            _movePosition = (_linePositions[0] + _linePositions[1]) / 2;
            _movePosition.x = Rocket.position.x;
        }

        _movePosition.x = Mathf.Clamp(_movePosition.x, -3f, 3f);
        Rocket.position = Vector3.MoveTowards(Rocket.position, _movePosition, _speedOnLastUpdate);
    }

    private void SetRocketRotation()
    {
        float signature = _linePositions[0].y > _linePositions[1].y ? 1f : -1f;
        float distance = Mathf.Abs(_linePositions[0].y - _linePositions[1].y);

        if (Mathf.Approximately(distance, 0))
        {
            Rocket.rotation = Quaternion.identity;
        }
        else
        {
            float normalizedValue = signature * NormalizeDistance(distance);

            Rocket.rotation = Quaternion.Euler(
                0f,
                0f,
                normalizedValue * 45f);
        }
    }

    private float NormalizeDistance(float distance)
    {
        float clampedDistance = Mathf.Clamp(distance, 0, MaxDistance);
        return clampedDistance / MaxDistance;
    }

    private Vector2 getWorldPoint(Vector3 screenPoint)
    {
        if (!Camera.main.orthographic)
        {
            //RaycastHit hit;
            //Physics.Raycast(Camera.main.ScreenPointToRay(screenPoint), out hit);
            //return hit.point;
            screenPoint.z = Camera.main.farClipPlane;
            screenPoint = Camera.main.ScreenToWorldPoint(screenPoint);
            screenPoint.Normalize();
            return screenPoint * 10;
        }
            return Camera.main.ScreenToWorldPoint(screenPoint);   
    }
}
