﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorScript : MonoBehaviour
{
    public float OutsideOfScreenY = 7f;
    public bool IsRotating = true;

    void Start()
    {
        if (!IsRotating)
            OutsideOfScreenY *= 2f;
    }

    void Update()
    {
        if (gameObject.transform.position.y >= OutsideOfScreenY)
            Destroy(gameObject);

        var translatePosition = Vector3.up * Time.deltaTime;
        gameObject.transform.Translate(translatePosition, Space.World);

        if(IsRotating)
        transform.Rotate(
            Time.deltaTime * Random.Range(10f, 30f),
            Time.deltaTime * Random.Range(10f, 30f),
            0);
    }
}
