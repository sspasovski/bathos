﻿using System.Collections;
using System.Collections.Generic;
using Me.StefS.UnityUtility;
using Me.StefS.UnityUtility.MVC;
using UnityEngine;
using UnityEngine.UI;

public class ItemPickupController : Controller
{
    PlayerModel playerModel;

    public List<GameObject> ObtainedItems;
    private Image itemToShow;

    protected override Model DefaultObjectWhenNoModelIsPresent()
    {
        return PlayerModel.ZERO_LEVEL;
    }

    protected override SceneType GeteSceneForWhichThisControllerIs()
    {
        return SceneType.ItemPickUpScene;
    }

    protected override void InitializeModel(Model model)
    {
        this.playerModel = (PlayerModel)model;
    }

    protected override IEnumerator OnStart()
    {
        if (playerModel.Level > ObtainedItems.Count)
            Navigator.GoToScene(SceneType.OutroScene, playerModel);

        itemToShow = ObtainedItems[playerModel.Level - 1].GetComponent<Image>();

        for (int i = 0; i < ObtainedItems.Count; i++)
        {
            if (this.playerModel.Level > i + 1)
            {
                var item = ObtainedItems[i].GetComponent<Image>();
                var tempColor = item.color;
                tempColor.a = 1f;
                item.color = tempColor;
            }
        }

        yield return null;
    }

    void Update()
    {
        if (itemToShow == null) return;

        if (!Mathf.Approximately(itemToShow.color.a, 1f))
        {
            var tempColor = itemToShow.color;
            tempColor.a += Time.deltaTime * 1 / 3f;
            itemToShow.color = tempColor;
        }

        if (Input.anyKeyDown)
        {
            playerModel.Level++;
            Navigator.GoToScene(SceneType.GravityPullScene, playerModel);
        }
    }
}
