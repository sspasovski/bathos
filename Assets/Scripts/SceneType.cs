﻿using Me.StefS.UnityUtility.MVC;

public enum SceneType {

    [Name("LoadingScene")]
    LoadingScene, //Logo

    [Name("TheAwakeningScene")]
    TheAwakeningScene, //Game Intro

    [Name("GravityPullScene")]
    GravityPullScene, //Mechanics

    [Name("AtmosphereEntranceScene")]   
    AtmosphereEntranceScene, //Cosmetics

    [Name("ShipTiltScene")]
    ShipTiltScene, //Mechanics

    [Name("LandingScene")]
    LandingScene, //Cosmetics

    [Name("ItemPickUpScene")]
    ItemPickUpScene, //Story Cosmetics

    [Name("EarthLandingScene")]
    EarthLandingScene, //Game Outro

    [Name("OutroScene")]
    OutroScene, //Team Outro

    [Name("DreamScene")]
    DreamScene
}
