﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Me.StefS.Utility;

public class GravityPullCalculator
{
    public static Vector3 calculatePull(List<Planet> planets, Vector3 calculatedPosition){
        Vector3 accumilatedPull = Vector3.zero;
        foreach(Planet planet in planets){
            accumilatedPull += GravityPullCalculator.forceByPlanet(planet, calculatedPosition);
        }

        return accumilatedPull;
    }

    private static Vector3 forceByPlanet(Planet planet, Vector3 calculatedPositon) {
        Vector3 objToPlanet = calculatedPositon.VectorTo(planet.position);
        float strength = (1 - GravityPullCalculator.NormalizeDistance(objToPlanet.magnitude)) * planet.mass;
        return objToPlanet.normalized * strength;
    }

    private static float NormalizeDistance(float distance) {
        float clampedDistance = Mathf.Clamp(distance, 0, Constants.MAX_GRAVITY_DECLINE);
        return clampedDistance / Constants.MAX_GRAVITY_DECLINE;
    }

    public static  float calculateRadius(float mass) {
        return mass / 3;
        //return Mathf.Pow((3 * mass) / (4 * Mathf.PI), 1f / 3f);
    }
}
