﻿using UnityEngine;
using Me.StefS.UnityUtility.MVC;
using System.Collections;
using Me.StefS.UnityUtility;

public class ShipTiltController : Controller
{
    PlayerModel playerModel;

    public Transform Background;

    private float _timeToPlay = -1f;
    private float _timeForBackground;

    private ObstacleSpawner _spawner;

    protected override Model DefaultObjectWhenNoModelIsPresent()
    {
        return PlayerModel.ZERO_LEVEL;
    }

    protected override SceneType GeteSceneForWhichThisControllerIs()
    {
        return SceneType.ShipTiltScene;
    }

    protected override void InitializeModel(Model model)
    {
        this.playerModel = (PlayerModel)model;
    }

    protected override IEnumerator OnStart()
    {
        this._timeForBackground = this._timeToPlay = 10f + this.playerModel.Level * 5f;
        this._spawner = FindObjectOfType<ObstacleSpawner>();
        _spawner.PlayerModel = this.playerModel;
        yield return null;
    }

    public void Update()
    {
        if (_timeToPlay == -1f) return;

        if (_timeToPlay < 0)
            Win();

        _timeToPlay -= Time.deltaTime;

        Vector3 tempBackgroundPosition = Background.position;
        tempBackgroundPosition.y += 30 * Time.deltaTime / (_timeForBackground/2);
        Background.position = tempBackgroundPosition;

    }

    public void Dead()
    {
        //reset scene
        Navigator.GoToScene(GeteSceneForWhichThisControllerIs(), playerModel);
    }

    public void Win()
    {
        // change Scene
        Navigator.GoToScene(SceneType.ItemPickUpScene, playerModel);
    }
}
