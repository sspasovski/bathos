﻿using UnityEngine;
using Me.StefS.UnityUtility.MVC;
using System.Collections;
using Me.StefS.UnityUtility;

public class LoadController : Controller {
    PlayerModel playerModel;
    protected override Model DefaultObjectWhenNoModelIsPresent() {
        return PlayerModel.ZERO_LEVEL;
    }

    protected override SceneType GeteSceneForWhichThisControllerIs() {
        return SceneType.LoadingScene;
    }

    protected override void InitializeModel(Model model) {
        this.playerModel = (PlayerModel) model;
    }

    protected override IEnumerator OnStart() {
        return Corutines.WaitForSeconds(3f).Then(Corutines.Execute(() => Navigator.GoToScene(SceneType.DreamScene, this.playerModel)));
    }

    public void Update()
    {
        if (Input.anyKey)
            Navigator.GoToScene(SceneType.DreamScene, this.playerModel);
    }
}