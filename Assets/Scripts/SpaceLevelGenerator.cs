﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceLevelGenerator
{
    private static float startY = -30;
    static Level[] levels = generateLevels();

    private static Level[] generateLevels()
    {
        List<Level> levels = new List<Level>();
        levels.Add(level1());
        levels.Add(level2());
        levels.Add(level3());
        levels.Add(level4());
        levels.Add(level5());
        return levels.ToArray();
    }

    //Done
    private static Level level1()
    {
        Level level = new Level();
        Planet home = new Planet();
        home.mass = 20f;
        home.position = new Vector3(0, startY, 0);
        level.StartPlanet = home;

        Planet[] others = new Planet[1];
        others[0] = new Planet();
        others[0].mass = 40f;
        others[0].position = new Vector3(0, 10, 0);
        level.otherPlanets = others;

        Planet hotEarth = new Planet();
        hotEarth.mass = 20f;
        hotEarth.position = new Vector3(0, 50, 0);
        level.TargetPlanet = hotEarth;
        return level;
    }


    //Done
    private static Level level2() {
        Level level = new Level();
        Planet home = new Planet();
        home.mass = 20f;
        home.position = new Vector3(20, startY, 0);
        level.StartPlanet = home;

        Planet[] others = new Planet[2];
        others[0] = new Planet();
        others[0].mass = 40f;
        others[0].position = new Vector3(-15, -10, 0);

        others[1] = new Planet();
        others[1].mass = 30f;
        others[1].position = new Vector3(15, 20, 0);
        level.otherPlanets = others;

        Planet hotEarth = new Planet();
        hotEarth.mass = 20f;
        hotEarth.position = new Vector3(-20, 50, 0);
        level.TargetPlanet = hotEarth;
        return level;
    }

    private static Level level4() {
        Level level = new Level();
        Planet home = new Planet();
        home.mass = 20f;
        home.position = new Vector3(0, startY, 0);
        level.StartPlanet = home;

        Planet[] others = new Planet[3];
        others[0] = new Planet();
        others[0].mass = 30f;
        others[0].position = new Vector3(0, -10, 0);

        others[1] = new Planet();
        others[1].mass = 30f;
        others[1].position = new Vector3(-15, 5, 0);

        others[2] = new Planet();
        others[2].mass = 30f;
        others[2].position = new Vector3(15, 40, 0);

        level.otherPlanets = others;

        Planet hotEarth = new Planet();
        hotEarth.mass = 40f;
        hotEarth.position = new Vector3(-20, 50, 0);
        level.TargetPlanet = hotEarth;
        return level;
    }

    private static Level level3()
    {
        Level level = new Level();
        Planet home = new Planet();
        home.mass = 10f;
        home.position = new Vector3(0, startY, 0);
        level.StartPlanet = home;

        Planet[] others = new Planet[10];

        others[0] = new Planet();
        others[0].mass = 15f;
        others[0].position = new Vector3(0, 3, 0);
        others[1] = new Planet();
        others[1].mass = 17f;
        others[1].position = new Vector3(13, 10, 0);


        others[2] = new Planet();
        others[2].mass = 25f;
        others[2].position = new Vector3(5, 20, 0);
        others[3] = new Planet();
        others[3].mass = 40f;
        others[3].position = new Vector3(21, 20, 0);

        //the small ones
        others[4] = new Planet();
        others[4].mass = 3f;
        others[4].position = new Vector3(-19, -1, 0);
        others[5] = new Planet();
        others[5].mass = 4f;
        others[5].position = new Vector3(-12, -3, 0);
        others[6] = new Planet();
        others[6].mass = 6f;
        others[6].position = new Vector3(-22, -5, 0);
        others[7] = new Planet();
        others[7].mass = 2f;
        others[7].position = new Vector3(-18, -8, 0);
        others[8] = new Planet();
        others[8].mass = 3f;
        others[8].position = new Vector3(-20, -4, 0);
        others[9] = new Planet();
        others[9].mass = 4f;
        others[9].position = new Vector3(-18, -7, 0);



        level.otherPlanets = others;


        Planet hotEarth = new Planet();
        hotEarth.mass = 56f;
        hotEarth.position = new Vector3(0, 50, 0);
        level.TargetPlanet = hotEarth;
        return level;
    }


    private static Level dontLevel() {
        Level level = new Level();
        Planet home = new Planet();
        home.mass = 20f;
        home.position = new Vector3(0, startY, 0);
        level.StartPlanet = home;


        Planet[] others = new Planet[8];
        others[0] = new Planet();
        others[0].mass = 16f;
        others[0].position = new Vector3(-15, -25, 0);
        others[1] = new Planet();
        others[1].mass = 16f;
        others[1].position = new Vector3(15, -25, 0);

        others[2] = new Planet();
        others[2].mass = 16f;
        others[2].position = new Vector3(-15, -5, 0);
        others[3] = new Planet();
        others[3].mass = 16f;
        others[3].position = new Vector3(15, -5, 0);

        others[4] = new Planet();
        others[4].mass = 16f;
        others[4].position = new Vector3(-15, 15, 0);
        others[5] = new Planet();
        others[5].mass = 16f;
        others[5].position = new Vector3(15, 15, 0);

        others[6] = new Planet();
        others[6].mass = 16f;
        others[6].position = new Vector3(-15, 35, 0);
        others[7] = new Planet();
        others[7].mass = 16f;
        others[7].position = new Vector3(15, 35, 0);



        level.otherPlanets = others;


        Planet hotEarth = new Planet();
        hotEarth.mass = 20f;
        hotEarth.position = new Vector3(0, 50, 0);
        level.TargetPlanet = hotEarth;
        return level;
    }


    private static Level level5() {
        Level level = new Level();
        Planet home = new Planet();
        home.mass = 15f;
        home.position = new Vector3(-20, startY, 0);
        level.StartPlanet = home;


        Planet[] others = new Planet[10];
        others[0] = new Planet();
        others[0].mass = 34f;
        others[0].position = new Vector3(-20, 0, 0);
        others[1] = new Planet();
        others[1].mass = 12f;
        others[1].position = new Vector3(15, 32, 0);
        others[2] = new Planet();
        others[2].mass = 13f;
        others[2].position = new Vector3(13, 5, 0);
        others[3] = new Planet();
        others[3].mass = 11f;
        others[3].position = new Vector3(5, -4, 0);


        //the small ones
        level.otherPlanets = others;
        others[4] = new Planet();
        others[4].mass = 3f;
        others[4].position = new Vector3(-19, 52, 0);
        others[5] = new Planet();
        others[5].mass = 4f;
        others[5].position = new Vector3(-12, 32, 0);
        others[6] = new Planet();
        others[6].mass = 6f;
        others[6].position = new Vector3(-22, 35, 0);
        others[7] = new Planet();
        others[7].mass = 2f;
        others[7].position = new Vector3(-18, 40, 0);
        others[8] = new Planet();
        others[8].mass = 3f;
        others[8].position = new Vector3(-20, 47, 0);
        others[9] = new Planet();
        others[9].mass = 4f;
        others[9].position = new Vector3(-18, 30, 0);
  




        Planet hotEarth = new Planet();
        hotEarth.mass = 20f;
        hotEarth.position = new Vector3(20, 50, 0);
        level.TargetPlanet = hotEarth;
        return level;
    }

    public static Level getLevel(int levelIndex)
    {
        return levels[levelIndex - 1];
    }


}
