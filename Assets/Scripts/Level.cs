﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Me.StefS.Utility;

public class Level
{
    public Planet StartPlanet { get; set; }
    public Planet[] otherPlanets { get; set; }
    public Planet TargetPlanet { get; set; }

    List<Planet> planets;
    public List<Planet> GetPlanets(){
        if(planets == null){
            if(StartPlanet == null){
                throw new MissingReferenceException("Start planed must have been set");
            }

            planets = new List<Planet>();
            planets.Add(this.StartPlanet);
            if(otherPlanets != null){
                planets.AddAll(otherPlanets);
            }

            planets.Add(this.TargetPlanet);
        }

        return planets;
    }
}
