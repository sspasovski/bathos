﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Me.StefS.UnityUtility.MVC;
using System;

public class PlayerModel : Model {
    public static PlayerModel ZERO_LEVEL = createZeroLevelPlayer();

    private static PlayerModel createZeroLevelPlayer() {
        PlayerModel model = new PlayerModel();
        model.Level = 1;
        return model;
    }

    public int Level { get; set; }

    public override IEnumerator Initialize() {
        yield return null;
    }
}
