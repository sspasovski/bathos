﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet
{
    public Vector3 position { get; set; }
    public float mass { get; set; }
    public float ringStartDistance { get; set; }
    public float ringEndDistance { get; set; }
    public Moon[] moons;
}
