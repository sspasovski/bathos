﻿using System.Collections;
using System.Collections.Generic;
using Me.StefS.UnityUtility;
using Me.StefS.UnityUtility.MVC;
using UnityEngine;

public class AtmosphereEntranceController : Controller
{
    PlayerModel playerModel;

    protected override Model DefaultObjectWhenNoModelIsPresent()
    {
        return PlayerModel.ZERO_LEVEL;
    }

    protected override SceneType GeteSceneForWhichThisControllerIs()
    {
        return SceneType.AtmosphereEntranceScene;
    }

    protected override void InitializeModel(Model model)
    {
        this.playerModel = (PlayerModel)model;
    }

    protected override IEnumerator OnStart()
    {
        Navigator.GoToScene(SceneType.ShipTiltScene, this.playerModel);
        yield return null;
    }

}
